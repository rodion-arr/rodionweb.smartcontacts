<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixCatalogSmartFilter $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;

if(!CModule::IncludeModule("rodionweb.smartcontacts"))
{
	ShowError(GetMessage("MODULE_NOT_INSTALLED"));
	return;
}
$arResult = array();

$strPhones = $this->phonesOption;
$arPhones = explode("\r\n", $strPhones);

if(count($arPhones) > 0)
{
    foreach ($arPhones as $strPhone)
    {
        $arResult["PHONES"][] = array(
            "TEXT" => $strPhone,
            "HREF" => $this->getHrefPhone($strPhone)
        );
    }
}

$this->IncludeComponentTemplate();
