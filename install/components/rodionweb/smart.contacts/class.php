<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class CSmartContacts extends CBitrixComponent
{
    public $phonesOption = "";

	public function executeComponent()
    {
        $this->phonesOption = COption::GetOptionString("rodionweb.smartcontacts", "PHONE_VALUE");

        return parent::executeComponent();
    }

    public function getHrefPhone($strPhone)
    {
        return str_replace(
            array('(', ')', ' ', '-'),
            "",
            $strPhone
        );
    }
}
