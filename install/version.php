<?php
/**
 * User: Rodion Abdurakhimov
 * Mail: rodion.arr.web@gmail.com
 * Date: 10/02/15
 */

$arModuleVersion = array(
    "VERSION" => "1.0.0",
    "VERSION_DATE" => "2015-06-04 00:00:00"
);