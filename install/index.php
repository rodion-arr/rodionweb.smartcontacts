<?php
/**
 * User: Rodion Abdurakhimov
 * Mail: rodion.arr.web@gmail.com
 * Date: 10/02/15
 */
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if(class_exists("rodionweb_smartcontacts"))
    return;

class rodionweb_smartcontacts extends CModule
{
    var $MODULE_ID = "rodionweb.smartcontacts";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function __construct()
    {
        $arModuleVersion = array();

        include dirname(__FILE__)."/version.php";

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];


        $this->MODULE_NAME = Loc::getMessage('CONTACTS_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('CONTACTS_MODULE_DESCRIPTION');

        $this->PARTNER_NAME = "rodion-web"; 
        $this->PARTNER_URI = "http://rodion-web.pp.ua";
    }

    function InstallDB()
    {
        RegisterModule("rodionweb.smartcontacts");

        return true;
    }

    function UnInstallDB()
    {
        UnRegisterModule("rodionweb.smartcontacts");

        return true;
    }

    function InstallFiles()
    {
        CopyDirFiles(dirname(__FILE__)."/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx('/bitrix/components/rodionweb/smart.contacts');
        return true;
    }

    function DoInstall()
    {
        $this->InstallFiles();
        $this->InstallDB();
    }

    function DoUninstall()
    {
        $this->UnInstallFiles();
        $this->UnInstallDB();
    }
}
